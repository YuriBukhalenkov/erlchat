-module(storage).
-author("yuri").

-behaviour(gen_server).


%% API
-export([
	start_link/0,
	checkin/2,
	checkout/2,
	say/3,
	list/1 %% for debug
]).

%% gen_server callbacks
-export([
	init/1,
	handle_call/3,
	handle_cast/2,
	handle_info/2,
	terminate/2,
	code_change/3
]).


-record(room, {
	name      :: binary(),
	users     :: map(),     %% #{name => pid}
	chat = [] :: [binary()]
}).

-record(state, {
	rooms_tbl :: pid()
}).



%%---------------------------
%% API Implementation

start_link() ->
	gen_server:start_link( {local, ?MODULE}, ?MODULE, [], [] ).


checkin( Who, Room ) ->
	gen_server:cast( ?MODULE, {checkin, {Who, self()}, Room} ).


checkout( Who, Room ) ->
	gen_server:cast( ?MODULE, {checkout, Who, Room} ).


say( Who, Room, Msg ) ->
	gen_server:cast( ?MODULE, {say, Who, Room, Msg} ).


list( Tab ) ->
	ets:tab2list( Tab ).


%%---------------------------
%% gen_server callbacks

init( [] ) ->
	lager:info( "~p started", [?MODULE] ),
	{ok, #state{
		rooms_tbl = ets:new(rooms, [{keypos, #room.name}])
	}}.


handle_call( Request, From, State ) ->
	lager:warning( "!!!UNKNOWN CALL: ~p from ~p", [Request, From] ),
	{reply, ok, State}.


handle_cast( {checkin, {Who, Pid}, Room}, #state{rooms_tbl = Tab} = State ) ->
	RoomNew = case ets:lookup(Tab, Room) of
		[#room{users = Users, chat = Phrases} = R] ->
			reply( Pid, lists:reverse(lists:sublist(Phrases, 10)) ),
			R#room{users = Users#{Who => Pid}};

		[]  ->
			reply( Pid, <<Room/binary, " constructed. There is no a single soul here.">> ),
			#room{name = Room, users = #{Who => Pid}}
	end,
	ets:insert( Tab, RoomNew ),
	{noreply, State};

handle_cast( {checkout, Who, Room}, #state{rooms_tbl = Tab} = State ) ->
	case ets:lookup(Tab, Room) of
		[#room{users = Users, chat = Phrases} = R] ->
			%% remove user from room
			case maps:remove(Who, Users) of
				Empty when map_size(Empty) == 0 ->
					ets:delete( Tab, Room );

				UsersUpd ->
					%% say bye to others
					Msg = <<Who/binary, " has left">>,

					reply( maps:values(UsersUpd), Msg ),

					ets:insert( Tab, R#room{users = UsersUpd, chat = [Msg | Phrases]} )
			end;

		_ ->
			ok
	end,
	{noreply, State};

handle_cast( {say, Who, any, Msg}, #state{rooms_tbl = Tab} = State ) ->
	case ets:tab2list( Tab ) of
		[] -> ok;

		[#room{} = Room] -> say( Tab, Who, Room, Msg );

		Rooms ->
			say(
				Tab,
				Who,
				lists:nth( rand:uniform(length(Rooms)), Rooms ),
				Msg
			)
	end,
	{noreply, State};

handle_cast( {say, Who, Room, Msg}, #state{rooms_tbl = Tab} = State ) when is_binary(Room) ->
	case ets:lookup( Tab, Room ) of
		[#room{} = R] -> say( Tab, Who, R, Msg );
		_             -> ok
	end,
	{noreply, State};

handle_cast( Request, State ) ->
	lager:warning( "!!!UNKNOWN CAST: ~p", [Request] ),
	{noreply, State}.


handle_info( Info, State ) ->
	lager:warning( "UNKNOWN MESSAGE: ~p", [Info] ),
	{noreply, State}.


terminate( Reason, _State ) ->
	lager:debug( "terminate: ~p", [Reason] ),
	ok.


code_change( OldVsn, State, _Extra ) ->
	lager:info( "UPGRADE version from ~p", [OldVsn] ),
	{ok, State}.



%%---------------------------
%% internal

say( Tab, Who, #room{users = Users, chat = Phrases} = R, Msg ) ->
	MsgNew = <<"[", Who/binary, "] : ", Msg/binary>>,

	case maps:remove(Who, Users) of
		Empty when map_size(Empty) == 0 -> ok;
		UsersUpd -> reply( maps:values(UsersUpd), MsgNew )
	end,

	ets:insert( Tab, R#room{chat = [MsgNew | Phrases]} ).

reply( Pid, Msgs ) when is_pid(Pid), is_list(Msgs) ->
	Pid ! {update, Msgs};

reply( Pid, Msg ) when is_pid(Pid), is_binary(Msg) ->
	reply( Pid, [Msg] );

reply( Pids, Msg ) when is_list(Pids) ->
	lists:foreach(
		fun ( Pid ) when is_pid(Pid) ->
			reply( Pid, Msg )
		end,
		Pids
	).