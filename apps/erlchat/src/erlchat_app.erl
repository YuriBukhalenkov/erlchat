-module(erlchat_app).
-author("yuri").

-behaviour(application).

-define(APP, erlchat).

%% API
-export([
	start/0,
	connections/0
]).


%% Application callbacks
-export([
	start/2,
	stop/1
]).


%%---------------------------
%% API

start() ->
	application:ensure_all_started( ?APP ).


connections() ->
	ranch:procs( server, connections ).


%%---------------------------
%% Application callbacks

start( _StartType, _StartArgs ) ->
	{ok, #{port := SrvPort}} = application:get_env(?APP, server),

	SrvOpts  = #{socket_opts => [{port, SrvPort}], num_acceptors => 32, max_connections => infinity},

	Dispatch = cowboy_router:compile(
		[{'_', [
			{"/ws/[...]", ws_chat_handler, []},
			{"/", cowboy_static, {priv_file, ?APP, "index.html"}},
			{"/[...]", cowboy_static, {priv_dir, ?APP, ""}}
		]}]
	),

	case cowboy:start_clear(server, SrvOpts, #{env => #{dispatch => Dispatch}}) of
		{ok, _} ->
		lager:info("Listen ports: ~p", [SrvPort]),
			erlchat_sup:start_link(); %% {ok, pid()}

		Error ->
			lager:error( "Failed listen http port ~p with: ~p", [SrvPort, Error] ),
			{error, {bad_tcp_port, Error}}
	end.


stop( _State ) ->
	ok = cowboy:stop_listener( server ).