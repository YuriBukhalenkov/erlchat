-module(bot_ctrl).
-author("yuri").

-define(SERVER,                ?MODULE).
-define(APP,                   erlchat).
-define(REPLICAS_FNAME, "bot.replicas").
-define(DEFAULT_WAKEUP_TIME,        60).
-define(MAX_WAKEUP_TIME,           300).

-behaviour(gen_server).

%% API
-export([
	start_link/1
]).

%% gen_server callbacks
-export([
	init/1, 
	handle_call/3, 
	handle_cast/2, 
	handle_info/2, 
	terminate/2, 
	code_change/3
]).


-record(state, {
	replicas     :: {list(), list()}, %% {Waited, Passed}
	wakeup_timer :: reference()
}).


%%-----------------------------------------------------------------------------
%% API

start_link( Cfg ) when is_map(Cfg) ->
	gen_server:start_link( {local, ?SERVER}, ?MODULE, Cfg, [] ).


%%-----------------------------------------------------------------------------
%% gen_server callbacks

init( Cfg ) ->
	rand:seed( exsss ),

	Time = maps:get( wakeup, Cfg, ?DEFAULT_WAKEUP_TIME ),

	lager:info( "~p started with ~p", [?MODULE, Cfg] ),

	{ok, #state{
		replicas     = load_replicas(),
		wakeup_timer = wakeup_async( 1000 * min(Time, ?MAX_WAKEUP_TIME) )
	}}.


handle_call( Request, From, State ) ->
	lager:warning( "!!!UNKNOWN CALL: ~p from ~p", [Request, From] ),
	{reply, ok, State}.


handle_cast( Request, State ) ->
	lager:warning( "!!!UNKNOWN CAST: ~p", [Request] ),
	{noreply, State}.


handle_info( {wakeup, Interval}, #state{wakeup_timer = Timer} = State ) ->
	erlang:cancel_timer( Timer ),

	case random_replica(State#state.replicas) of
		{error, _} ->
			lager:warning( "no phrases for bot found" ),
			{stop, normal, State};

		{{Person, Msg}, Replicas} ->
			lager:info( "~p as bot says: ~p", [Person, Msg] ),
			storage:say( Person, any, Msg ),
			{noreply, State#state{
				replicas     = Replicas,
				wakeup_timer = wakeup_async( Interval )
			}}
	end;

handle_info( Info, State ) ->
	lager:warning( "UNKNOWN MESSAGE: ~p", [Info] ),
	{noreply, State}.


terminate( Reason, _State ) ->
	lager:debug( "terminate: ~p", [Reason] ),
	ok.


code_change( OldVsn, State, _Extra ) ->
	lager:info( "UPGRADE version from ~p", [OldVsn] ),
	{ok, State}.


%%-----------------------------------------------------------------------------
%% Internal functions

wakeup_async( Interval ) when is_integer(Interval) ->
	erlang:send_after( random_period(Interval), self(), {wakeup, Interval} ).


random_period( MidPeriod ) when is_integer(MidPeriod) ->
	(MidPeriod bsr 1) + rand:uniform( MidPeriod ) - 1.


load_replicas() ->
	Res = case file:consult( filename:join(priv_dir(), ?REPLICAS_FNAME) ) of
		{ok, Terms}     -> Terms;
		{error, Reason} -> lager:error( "failed to parse bot phrases with ~p", [Reason] ), []
	end,
	{Res, []}.


priv_dir() ->
	case code:priv_dir(?APP) of
		{error, bad_name} ->
			{ok, Cwd} = file:get_cwd(),
			Cwd ++ "/priv";

		Priv -> Priv
	end.


random_replica( {[], []} = R ) ->
	{error, R};

random_replica( {[], Passed} ) ->
	random_replica( {Passed, []} );

random_replica( {Waited, Passed} ) ->
	R = lists:nth( rand:uniform(length(Waited)), Waited ),
	{R, {lists:delete(R, Waited), [R | Passed]}}.