-module(erlchat_sup).
-author("yuri").

-behaviour(supervisor).

-define(SERVER, ?MODULE).
-define(APP,    erlchat).


%% API
-export([
	start_link/0
]).


%% supevisor callbacks
-export([
	init/1
]).


%%---------------------------
%% API Implementation

start_link() ->
	supervisor:start_link( {local, ?SERVER}, ?MODULE, [] ).


%%---------------------------
%% Sypervisor callbacks

init( [] ) ->
	ChildSpecs = [
		#{id => bot_ctrl, start => {bot_ctrl, start_link, config(bot)}},
		#{id => storage, start => {storage, start_link, []}}
	],

	SupFlags = #{
		strategy  => one_for_all,
		intensity => 0,
		period    => 1
	},

	{ok, {SupFlags, ChildSpecs}}.


config( Name ) ->
	{ok, Cfg} = application:get_env( ?APP, Name ),
	[Cfg].