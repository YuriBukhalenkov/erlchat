-module(ws_chat_handler).
-author("yuri").

%% API
-export([
	init/2,
	websocket_init/1,
	websocket_handle/2,
	websocket_info/2,
	terminate/3
]).


-record(state, {
	user :: binary(),
	room :: binary()
}).


%%---------------------------
%% API Implementation

init( #{path_info := [Room, User], peer := Peer} = Req, _Opts ) ->
	lager:info( "Getting request(~p) from: ~p", [cowboy_req:path(Req), Peer] ),

	{cowboy_websocket, Req, #state{user = User, room = Room}}.


websocket_init( #state{user = User, room = Room} = State ) ->
	storage:checkin( User, Room ),
	{[], State}.


websocket_handle( {text, Msg}, #state{user = User, room = Room} = State ) ->
	storage:say( User, Room, Msg ),
	{[{text, <<"[me] : ", Msg/binary>>}], State}.


websocket_info( {update, Replicas}, State ) when is_list(Replicas) ->
	lager:info( "websocket_handle update: ~p, state: ~p", [Replicas, State] ),
	{[{text, R} || R <- Replicas, is_binary(R)], State};


websocket_info( Info, State ) ->
	lager:warning( "websocket_handle UNHANDLED: ~p, state: ~p", [Info, State] ),
	{[], State}.


terminate( Reason, _Req, #state{user = User, room = Room} ) ->
	lager:info( "terminate with: ~p", [Reason] ),
	storage:checkout( User, Room ),
	ok.