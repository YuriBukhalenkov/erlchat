erlchat
=====

An OTP application

Build
-----

    $ rebar3 compile


Run
-----

	$ rebar3 shell
	$ > erlchat_app:start().
